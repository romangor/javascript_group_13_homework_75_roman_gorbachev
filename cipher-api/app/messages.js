const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const router = express.Router();

router.post('/:word', (req, res) => {
  const message = {...req.body};
  if(req.params.word === 'encode'){
    const encode = Vigenere.Cipher(message.password).crypt(message.message);
    const response = {
      "encoded": encode
    }
    return  res.send(response);
  }
  if(req.params.word === 'decode'){
    const decode = Vigenere.Decipher(message.password).crypt(message.message);
    const response = {
      "decoded": decode
    }
    return  res.send(response);
  }
});


module.exports = router;