import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Message } from './message.model';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class MessagesService  {
  encodedWord = new Subject<string>();
  decodedWord = new Subject<string>();

  constructor(private http: HttpClient) { }

  sendMessage(message: Message){
    const body = {
      password: message.password,
      message: message.message,
    }
    this.http.post('http://localhost:8000/' + message.path, body).subscribe( response => {
      const value = Object.values(response);
      const key = Object.keys(response);
      if(key[0] === 'encoded'){
        this.decodedWord.next(value[0]);
      }
      if(key[0] === 'decoded'){
        this.encodedWord.next(value[0]);
      }
    });
  }
}
