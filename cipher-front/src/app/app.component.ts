import { Component, OnInit } from '@angular/core';
import { MessagesService } from './shared/message.service';
import { Message } from './shared/message.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit{

  encodedWord!: string;
  decodedWord!: string;
  inputPassword!: string;

  constructor(private messagesService: MessagesService) {}

  ngOnInit() {
    this.messagesService.decodedWord.subscribe( word => {
      this.encodedWord = word;
    });
    this.messagesService.encodedWord.subscribe( word => {
      this.decodedWord = word;
    });
  }

  onCipher(path: string, e:Event) {
    e.preventDefault();
    let messageText: string = '';
    if(path === 'encode'){
      messageText =  this.decodedWord;
    }
    if(path === 'decode'){
      messageText =  this.encodedWord;
    }
    const message = new Message(
      this.inputPassword,
      messageText,
      path
    );
    this.messagesService.sendMessage(message);
  }
}
